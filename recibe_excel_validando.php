<?php
require('config.php');
$tipo       = $_FILES['dataCliente']['type'];
$tamanio    = $_FILES['dataCliente']['size'];
$archivotmp = $_FILES['dataCliente']['tmp_name'];
$lineas     = file($archivotmp);

$i = 0;

foreach ($lineas as $linea) {
    $cantidad_registros = count($lineas);
    $cantidad_regist_agregados =  ($cantidad_registros - 1);

    if ($i != 0) {

        $datos = explode(";", $linea);
       
        $cui                = !empty($datos[0])  ? ($datos[0]) : '';
		$nombre               = !empty($datos[1])  ? ($datos[1]) : '';
        $apellido1               = !empty($datos[2])  ? ($datos[2]) : '';
        $apellido2               = !empty($datos[3])  ? ($datos[3]) : '';
       
if( !empty($cui) ){
    $checkemail_duplicidad = ("SELECT cui FROM usuario WHERE cui='".($cui)."' ");
            $ca_dupli = mysqli_query($con, $checkemail_duplicidad);
            $cant_duplicidad = mysqli_num_rows($ca_dupli);
        }   

//No existe Registros Duplicados
if ( $cant_duplicidad == 0 ) { 

$insertarData = "INSERT INTO usuario( 
   cui,
    nombre,
    apellido1,
    apellido2
) VALUES(
    '$cui',
    '$nombre',
    '$apellido1',
    '$apellido2'
)";
mysqli_query($con, $insertarData);


        
} 
/**Caso Contrario actualizo el o los Registros ya existentes*/
else{
    $updateData =  ("UPDATE usuario SET 
        cui='" .$cui. "',
		nombre='" .$nombre. "',
        apellido1='" .$apellido1. "'
        apellido2='" .$apellido2. "'  
        WHERE cui='".$cui."'
    ");
    $result_update = mysqli_query($con, $updateData);
    } 
  }

 $i++;
}


?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link type="text/css" rel="shortcut icon" href="img/logo_umg.jpg"/>
  <title>Importando datos</title>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/material-design-iconic-font/2.2.0/css/material-design-iconic-font.min.css">
  <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="css/cargando.css">
  <link rel="stylesheet" type="text/css" href="css/cssGenerales.css">
</head>
<body>

 <div class="cargando">
    <div class="loader-outter"></div>
    <div class="loader-inner"></div>
</div> 


<nav class="navbar navbar-expand-lg navbar-light navbar-dark fixed-top" style="background-color: #fff !important;">
    <ul class="navbar-nav mr-auto collapse navbar-collapse">
      <li class="nav-item active">
        <a href="index.php"> 
          <img src="img/logo_umg.jpg" alt="Importando datos" width="120">
        </a>
      </li>
    </ul>
    <div class="my-2 my-lg-0">
      <h5 class="navbar-brand" style="color:#FF0000">Importando datos</h5>
    </div>
</nav>


<div class="container">

<h3 class="text-center">
    Datos Importados
</h3>
<hr>
<br><br>




    <div class="col-md-5">
  <?php
  header("Content-Type: text/html;charset=utf-8");
  include('config.php');
  $sqlClientes = ("SELECT * FROM usuario ORDER BY cui ASC");
  $queryData   = mysqli_query($con, $sqlClientes);
  $total_client = mysqli_num_rows($queryData);
  ?>

      <h6 class="text-center">
        Prueba Lista de Clientes Cargados <strong>(<?php echo $total_client; ?>)</strong>
      </h6>

        <table class="table table-bordered table-striped">
          <thead>
            <tr>
              <th>#</th>
               <th>cui</th>
              <th>nombre</th>
              <th>apellido1</th>
              <th>apellido2</th>
            </tr>
          </thead>
          <tbody>
            <?php 
            $i = 1;
            while ($data = mysqli_fetch_array($queryData)) { ?>
            <tr>
              <th scope="row"><?php echo $i++; ?></th>
              <td><?php echo $data['cui']; ?></td>
              <td><?php echo $data['nombre']; ?></td>
              <td><?php echo $data['apellido1']; ?></td>
              <td><?php echo $data['apellido2']; ?></td>
            </tr>
          <?php } ?>
          </tbody>
        </table>

    </div>
  </div>

</div>


<script src="js/jquery.min.js"></script>
<script src="'js/popper.min.js"></script>
<script src="js/bootstrap.min.js"></script>

<script type="text/javascript">
    $(document).ready(function() {
        $(window).load(function() {
            $(".cargando").fadeOut(1000);
        });      
});
</script>

</body>
</html>